﻿using System;
using System.Collections.Generic;

namespace Shop.EF.Model
{
    public partial class Collect
    {
        public int CollectId { get; set; }
        public int? ProductId { get; set; }
        public int? UserId { get; set; }
    }
}
