﻿using System;
using System.Collections.Generic;

namespace Shop.EF.Model
{
    public partial class Address
    {
        public int AddressId { get; set; }
        public int? UserId { get; set; }
        public string Addressinfo { get; set; }
        public string Isdefault { get; set; }
        public string UserPhone { get; set; }
        public string Addressarea { get; set; }
        public string Sname { get; set; }
    }
}
