﻿using System;
using System.Collections.Generic;

namespace Shop.EF.Model
{
    public partial class Orders
    {
        public int OrderId { get; set; }
        public int? ProductId { get; set; }
        public decimal? Price { get; set; }
        public int? Ocount { get; set; }
        public decimal? TotalPrice { get; set; }
        public int? UserId { get; set; }
        public int? AddressId { get; set; }
        public int? CartId { get; set; }
        public int? Stauts { get; set; }
        public string Flag { get; set; }
        public string BuidTime { get; set; }
        public string Orderno { get; set; }
    }
}
