﻿using System;
using System.Collections.Generic;

namespace Shop.EF.Model
{
    public partial class LhgCity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Fename { get; set; }
        public string Ename { get; set; }
        public int Pid { get; set; }
        public int Level { get; set; }
        public int Region { get; set; }
        public string MapX { get; set; }
        public string MapY { get; set; }
        public string MapZ { get; set; }
        public sbyte Orders { get; set; }
        public sbyte Status { get; set; }
        public sbyte Istop { get; set; }
        public int CityId { get; set; }
        public int CityArea { get; set; }
    }
}
