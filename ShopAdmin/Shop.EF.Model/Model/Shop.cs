﻿using System;
using System.Collections.Generic;

namespace Shop.EF.Model
{
    public partial class Shop
    {
        public int ShopId { get; set; }
        public string ShopName { get; set; }
        public string ShopAddress { get; set; }
        public int? CategoryId { get; set; }
    }
}
