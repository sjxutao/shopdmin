﻿using System;
using System.Collections.Generic;

namespace Shop.EF.Model
{
    public partial class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string LoginPassword { get; set; }
        public string PayPassword { get; set; }
        public string UserNumber { get; set; }
        public string UserPhoto { get; set; }
        public string VerificationCode { get; set; }
    }
}
