﻿using System;
using System.Collections.Generic;

namespace Shop.EF.Model
{
    public partial class Product
    {
        public int ProductId { get; set; }
        public int? CategoryId { get; set; }
        public int? ShopId { get; set; }
        public string ProductName { get; set; }
        public decimal? ProductPrice { get; set; }
        public string ProductImgUrl { get; set; }
        public decimal? ProductUprice { get; set; }
        public string ProductNum { get; set; }
        public string ProductDetail { get; set; }
        public int? ProductCommentNum { get; set; }
        public int? ProductHot { get; set; }
        public int? ProductSpecial { get; set; }
        public int? ProductSearchHot { get; set; }
    }
}
