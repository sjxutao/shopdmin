﻿using System;
using System.Collections.Generic;

namespace Shop.EF.Model
{
    public partial class Category
    {
        public int CategoryId { get; set; }
        public int? CategoryPid { get; set; }
        public string CategoryName { get; set; }
    }
}
