﻿using System;
using System.Collections.Generic;

namespace Shop.EF.Model
{
    public partial class ProductImage
    {
        public int ImageId { get; set; }
        public int? ProductId { get; set; }
        public string ImageUrl { get; set; }
    }
}
