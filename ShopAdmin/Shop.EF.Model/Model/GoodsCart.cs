﻿using System;
using System.Collections.Generic;

namespace Shop.EF.Model
{
    public partial class GoodsCart
    {
        public int CartId { get; set; }
        public int? UserId { get; set; }
        public int? ProductId { get; set; }
        public int? GoodsNum { get; set; }
        public string Created { get; set; }
        public sbyte? Deleted { get; set; }
    }
}
