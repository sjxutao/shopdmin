﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Shop.EF.Model
{
    public partial class shopdbContext : DbContext
    {
        public shopdbContext()
        {
        }

        public shopdbContext(DbContextOptions<shopdbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Collect> Collect { get; set; }
        public virtual DbSet<GoodsCart> GoodsCart { get; set; }
        public virtual DbSet<LhgCity> LhgCity { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductImage> ProductImage { get; set; }
        public virtual DbSet<Shop> Shop { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;userid=root;pwd=root;port=3306;database=shopdb;sslmode=none;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("address");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.AddressId)
                    .HasColumnName("address_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Addressarea)
                    .HasColumnName("addressarea")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.Addressinfo)
                    .HasColumnName("addressinfo")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.Isdefault)
                    .HasColumnName("isdefault")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.Sname)
                    .HasColumnName("sname")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserPhone)
                    .HasColumnName("user_phone")
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("category");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("category_id")
                    .HasColumnType("int(100)");

                entity.Property(e => e.CategoryName)
                    .HasColumnName("category_name")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.CategoryPid)
                    .HasColumnName("category_pid")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Collect>(entity =>
            {
                entity.ToTable("collect");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.CollectId)
                    .HasColumnName("collect_id")
                    .HasColumnType("int(100)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<GoodsCart>(entity =>
            {
                entity.HasKey(e => e.CartId)
                    .HasName("PRIMARY");

                entity.ToTable("goods_cart");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.CartId)
                    .HasColumnName("cart_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(4)");

                entity.Property(e => e.GoodsNum)
                    .HasColumnName("goods_num")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<LhgCity>(entity =>
            {
                entity.ToTable("lhg_city");

                entity.HasIndex(e => e.Ename)
                    .HasName("ename");

                entity.HasIndex(e => e.Id)
                    .HasName("id");

                entity.HasIndex(e => e.Name)
                    .HasName("name");

                entity.HasIndex(e => e.Orders)
                    .HasName("orders");

                entity.HasIndex(e => e.Pid)
                    .HasName("pid");

                entity.HasIndex(e => e.Status)
                    .HasName("status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CityArea)
                    .HasColumnName("city_area")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.CityId)
                    .HasColumnName("city_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ename)
                    .IsRequired()
                    .HasColumnName("ename")
                    .HasColumnType("varchar(40)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Fename)
                    .IsRequired()
                    .HasColumnName("fename")
                    .HasColumnType("varchar(2)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Istop)
                    .HasColumnName("istop")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Level)
                    .HasColumnName("level")
                    .HasColumnType("int(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.MapX)
                    .IsRequired()
                    .HasColumnName("map_x")
                    .HasColumnType("varchar(50)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.MapY)
                    .IsRequired()
                    .HasColumnName("map_y")
                    .HasColumnType("varchar(50)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.MapZ)
                    .IsRequired()
                    .HasColumnName("map_z")
                    .HasColumnType("varchar(5)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(30)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Orders)
                    .HasColumnName("orders")
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Pid)
                    .HasColumnName("pid")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Region)
                    .HasColumnName("region")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'1'");
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.HasKey(e => e.OrderId)
                    .HasName("PRIMARY");

                entity.ToTable("orders");

                entity.HasIndex(e => e.AddressId)
                    .HasName("address_id");

                entity.HasIndex(e => e.CartId)
                    .HasName("cart_id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.OrderId)
                    .HasColumnName("order_id")
                    .HasColumnType("int(100)");

                entity.Property(e => e.AddressId)
                    .HasColumnName("address_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BuidTime)
                    .HasColumnName("buid_time")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.CartId)
                    .HasColumnName("cart_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Flag)
                    .HasColumnName("flag")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.Ocount)
                    .HasColumnName("ocount")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Orderno)
                    .HasColumnName("orderno")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Stauts)
                    .HasColumnName("stauts")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TotalPrice)
                    .HasColumnName("total_price")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("product");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(100)");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("category_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductCommentNum)
                    .HasColumnName("product_comment_num")
                    .HasColumnType("int(8)");

                entity.Property(e => e.ProductDetail)
                    .HasColumnName("product_detail")
                    .HasColumnType("text");

                entity.Property(e => e.ProductHot)
                    .HasColumnName("product_hot")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductImgUrl)
                    .HasColumnName("product_img_url")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.ProductNum)
                    .HasColumnName("product_num")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ProductPrice)
                    .HasColumnName("product_price")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.ProductSearchHot)
                    .HasColumnName("product_search_hot")
                    .HasColumnType("int(8)");

                entity.Property(e => e.ProductSpecial)
                    .HasColumnName("product_special")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductUprice)
                    .HasColumnName("product_uprice")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.ShopId)
                    .HasColumnName("shop_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ProductImage>(entity =>
            {
                entity.HasKey(e => e.ImageId)
                    .HasName("PRIMARY");

                entity.ToTable("product_image");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.Property(e => e.ImageId)
                    .HasColumnName("image_id")
                    .HasColumnType("int(100)");

                entity.Property(e => e.ImageUrl)
                    .HasColumnName("image_url")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(100)");
            });

            modelBuilder.Entity<Shop>(entity =>
            {
                entity.ToTable("shop");

                entity.Property(e => e.ShopId)
                    .HasColumnName("shop_id")
                    .HasColumnType("int(100)");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("category_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ShopAddress)
                    .HasColumnName("shop_address")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.ShopName)
                    .HasColumnName("shop_name")
                    .HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LoginPassword)
                    .IsRequired()
                    .HasColumnName("login_password")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.PayPassword)
                    .HasColumnName("pay_password")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("user_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.UserNumber)
                    .IsRequired()
                    .HasColumnName("user_number")
                    .HasColumnType("varchar(40)");

                entity.Property(e => e.UserPhoto)
                    .HasColumnName("user_photo")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.VerificationCode)
                    .HasColumnName("verification_code")
                    .HasColumnType("varchar(20)");
            });
        }
    }
}
