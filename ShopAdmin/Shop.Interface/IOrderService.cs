﻿using Service.tool.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Shop.Interface
{
    public interface IOrderService : IBaseService
    {
        PageResult<dynamic> GETOrderToAddressToProduct(int limit, int offset,string name=null);
        void UpdateOrderStatus(int orderId);
    }
}
