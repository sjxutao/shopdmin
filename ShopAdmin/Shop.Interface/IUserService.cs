﻿using Service.tool.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shop.Interface
{
    public interface IUserService : IBaseService
    {
        //void UpdateLastLogin(User user);

        //获取用户和默认地址信息
        PageResult<dynamic> GETUserToAddress();
    }
}
