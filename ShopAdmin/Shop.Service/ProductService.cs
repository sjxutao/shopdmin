﻿using Microsoft.EntityFrameworkCore;
using Shop.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shop.Service
{
    public class ProductService : BaseService, IProductService
    {
        public ProductService(DbContext context) : base(context)
        {
        }
    }
}
