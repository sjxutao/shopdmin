﻿using Microsoft.EntityFrameworkCore;
using Service.tool.Models;
using Shop.EF.Model;
using Shop.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;

namespace Shop.Service
{
    public class OrderService : BaseService, IOrderService
    {
        public OrderService(DbContext context) : base(context)
        {
        }
        /// <summary>
        /// 获取订单首页数据（订单，地址和产品）
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="funcWhere">查询条件</param>
        /// <param name="limit">取条数</param>
        /// <param name="offset">跳过条数</param>
        /// <param name="funcOrderby">排序字段</param>
        /// <param name="isAsc">排序</param>
        /// <returns></returns>
        public PageResult<dynamic> GETOrderToAddressToProduct(int limit, int offset,string name=null)
        {
            var list = (from o in this.Context.Set<Orders>()
                       join c in this.Context.Set<Address>() on o.AddressId equals c.AddressId
                       join d in this.Context.Set<Product>() on o.ProductId equals d.ProductId
                       join e in this.Context.Set<User>() on o.UserId equals e.UserId
                       select new 
                       {
                           OrderId=o.OrderId,
                           UserName = e.UserName,
                           ProductName = d.ProductName,
                           Price = o.Price,
                           Ocount = o.Ocount,
                           TotalPrice = o.TotalPrice,
                           Addressinfo = c.Addressinfo,
                           UserPhone = c.UserPhone,
                           Stauts = o.Stauts,
                           BuidTime = o.BuidTime
                       }).Skip(offset).Take(limit);
            if (name!=null)
            {
                list.Where(c => c.ProductName.Contains(name));
            }
            PageResult<dynamic> datalist = new PageResult<dynamic>();
            datalist.total = list.Count();
            datalist.rows = list.ToList<dynamic>();
            return datalist;
        }
        /// <summary>
        /// 更新订单状态
        /// </summary>
        /// <param name="user"></param>
        public void UpdateOrderStatus(int orderId)
        {
            Orders order = base.Find<Orders>(orderId);
            order.Stauts = 1;
            this.Commit();
        }
    }
}
