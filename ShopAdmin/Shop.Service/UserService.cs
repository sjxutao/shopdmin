﻿using Microsoft.EntityFrameworkCore;
using Service.tool.Models;
using Shop.EF.Model;
using Shop.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Shop.Service
{
    public class UserService : BaseService, IUserService
    {
        public UserService(DbContext context) : base(context)
        {
        }

        public PageResult<dynamic> GETUserToAddress()
        {
            var data =from o in this.Context.Set<User>()
                       join c in this.Context.Set<Address>() on o.UserId equals c.UserId
                       where c.Isdefault == "1"
                       select new
                       {
                           UserId = o.UserId,
                           UserName = o.UserName,
                           UserNumber = o.UserNumber,
                           Addressinfo = c.Addressinfo
                       };
            PageResult<dynamic> datalist = new PageResult<dynamic>();
            datalist.total = data.Count();
            datalist.rows = data.ToList<dynamic>();
            return datalist;
        }

        //public void UpdateLastLogin(User user)
        //{
        //    User userDB = base.Find<User>(user.Id);
        //    userDB.LastLoginTime = DateTime.Now;
        //    this.Commit();
        //}
    }
}
