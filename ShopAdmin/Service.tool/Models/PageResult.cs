﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.tool.Models
{
    public class PageResult<T>
    {
        public int total { get; set; }
        public List<T> rows { get; set; }
    }
}
